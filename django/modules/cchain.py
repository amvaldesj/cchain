#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#  cchain.py
#  
#  Copyright 2019 Alejandro Valdés Jiménez <amvaldesj@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  Concatenates chains from a PDB file.
#

import sys
import argparse
import ujson as json
import csv
import copy
import os
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Structure import Structure 
from Bio.PDB.Model import Model 
from Bio.PDB.Chain import Chain
from Bio.PDB import PDBIO
import re

class CCHAIN(object):
    def __init__(self, f_input, chains_selected, letter):
        """
        Parameters:

        f_input: str 
            The input PDB file.
        chains: str list
            The list of chains to concatenate, ['A','B']. If ['*'] is entered then all the chains will be concatenated.
        letter: str
            The letter of the new chain. If only one chain is entered, then the original chain name is used.
        """
        self.__f_input = f_input
        self.__chains = chains_selected
        self.__letter = letter
        
        self.__output_name = ""
        
        # was concatenated?
        self.concatenated = False
        
        # final chain name.
        self.uniq_chain = "A"
        
        # List of dictionaries that contains the chains with their residues.
        self.__res = []
        self.__new_struc = None
        self.__new_chain = None
        self.__residue_map = {}
        
        # list of chains entered has repeated elements?
        if (len(self.__chains) != (len(set(self.__chains)))):
            print ("There are chains repeated.")
            sys.exit()
        
        # replace "." by "_" in filename.
        self.__output_name =  os.path.join(os.path.dirname(f_input), os.path.basename(f_input).replace(".", "_"))
            
        # replace spaces by "_"
        self.__output_name = self.__output_name.replace(" ", "_")
        
        # parse PDB file.
        self.__parser = PDBParser()
        
        # get total structure.
        self.__structure = self.__parser.get_structure(self.__f_input , self.__f_input)
        
        # get first model.
        self.__model = self.__structure[0]
        
        # get object list of chains in the protein.
        all_chain = self.__model.get_list()
        
        # protein has only one chain.
        if (len(all_chain) == 1):
            print("only one chain in the protein. it is not necessary to concatenate. original chain name is selected.")
            
            # the chain (string)
            thechain = all_chain[0].get_id()
            
            self.uniq_chain = thechain
            self.concatenated = False
            
            # the chain (object). 
            new_chain = self.__model[thechain]
            
            # create the new structure.
            self.__create_new_structure(new_chain)
            
            #
            self.__no_concatenate(thechain)
        
        # protein has more than one chain.
        else:
            print("protein has more than one chain (", len(all_chain), ") ", chains_selected ," selected")
                
            # concatenate all the chains (*)?
            if (re.search(r"\*", chains_selected[0])):
                self.__chains = []
                # create a character list of chains.
                for a in all_chain:
                    self.__chains.append(a.id)
            
            # selected only one chain (not "*").
            if (len(self.__chains) == 1):
                # the chain (string)
                thechain = self.__chains[0]
                
                print("only one chain is selected: ", thechain)
                
                #
                self.__check_chain(thechain)
                
                self.uniq_chain = thechain
                self.concatenated = False
                
                # the chain (object). 
                new_chain = self.__model[thechain]
                
                # create the new structure.
                self.__create_new_structure(new_chain)
            
                #
                self.__no_concatenate(thechain)
            
            # concatenate chains.
            else:
                self.uniq_chain = self.__letter
                self.concatenated = True
                
                # sort the list.
                self.__chains.sort()
                
                print("chains selected to concatenate:", self.__chains, "on chain:", self.__letter)
            
                # check if the chains are valids.
                for ch in self.__chains:
                    #
                    self.__check_chain(ch)
                    
                    # get the chain's residues.
                    self.__res.append({"chain": ch, "residues": list(self.__model[ch].get_residues())})
                    
                #
                new_chain = Chain(self.__letter)
                
                # create the new structure.
                self.__create_new_structure(new_chain)
            
                # concatenate the chains.
                self.__concatenate()
        
        print("concatenated: ", self.concatenated)

    def get_chains(self):
        """
        Return a string list of chain selected.
        """
        return self.__chains
        
    def __check_chain(self, chain):
        """
        Check if the chain exist in the protein.
        """
        if (not chain in self.__model):
            print("Do not exist the chain: " + chain)
            sys.exit()

    def __create_new_structure(self, new_chain):
        """
        Create a new structure to store th chains of the new PDB file.
        """
        model = Model(0)
        self.__new_chain = new_chain
        model.add(self.__new_chain)
        self.__new_struc = Structure(0)
        self.__new_struc.add(model)

    def __no_concatenate(self, thechain):
        """
        Concatenation isn't realized, the original chain is parsed maintaining its number IDs.
        """
        # set name of output filename.
        self.__f_output =  self.__output_name + "-" + thechain

    def __concatenate(self):
        """
        Gets the residues/ligands of chains selected renumering its ID number.
        """
        # set name of output filename.
        self.__f_output =  self.__output_name + "-concat_" + "-".join(self.__chains) + "_" + self.__letter
        
        # new index.
        last_id = 1
        
        # every chain.
        for i in range(0, len(self.__chains)):
            self.__residue_map[self.__chains[i]] = []
            
            # get residues/ligands of the chain.
            residues = self.__res[i]["residues"]
            
            for res in residues:
                # a real copy of object.
                res_new = copy.copy(res)

                # changes the residue ID. IMPORTANT: ._id
                res_new._id = (res_new.id[0], last_id, res_new.id[2])
                
                # add residue to new chain.
                self.__new_chain.add(res_new)
                
                # add residue to map.
                self.__residue_map[self.__chains[i]].append({"origin": res.get_resname() + str(res.id[1]), "new": res.get_resname() + str(last_id)})
                
                last_id += 1
        
    def create_pdb_file(self):
        """
        Creates the new PDB file.
        """
        io = PDBIO()
        io.set_structure(self.__new_struc)
        io.save(self.__f_output + ".pdb")

    def create_json_file(self):
        """
        Creates the JSON file with the residues IDs map.
        """
        with open(self.__f_output + ".json", "w") as outfile:
            json.dump(self.__residue_map, outfile, indent=1)
    
    def create_csv_file(self):
        """
        Creates the CSV file with the residues IDs map.
        """
        with open(self.__f_output + ".csv", 'w') as f:
            f.write("%s,%s,%s\n"%('CHAIN', 'ORIGINAL', 'NEW'))
            for key, value in self.__residue_map.items():
                for data in value:
                    f.write("%s,%s,%s\n"%(key, data['origin'], data['new']))
    
    def get_full_output_filename(self):
        """
        Return the full output filename without extension (pdb or json)
        """
        return self.__f_output
    
    def get_output_filename(self):
        """
        Return filename only.
        """
        return self.__f_output.split("/")[-1]
    
def read_options ():
    """
    Return the parameters entered.
    """
    parser = argparse.ArgumentParser(description="Concatenates chains.")
    required = parser.add_argument_group('required arguments')
    required.add_argument('-i', '--input', metavar='PDB', dest='f_input', help='Input PDB file.', required=True)
    required.add_argument('-c', '--chains', metavar='CHAIN', dest='chains', help='List of chains to concatenate. Example: --chains B C. Use --chains \"*\" to concatenate all chains.', required=True, nargs='+')
    required.add_argument('-l', '--letter', metavar='CHAIN', dest='letter', help='Letter of the new chain (only if more than one chain is selected.)', required=True)
    return parser.parse_args()
        
if __name__ == '__main__':
    # $ python cchain.py -i 1tf6.pdb -c "*" -l A
    # $ python cchain.py -i 1tf6.pdb -c A -l A
    # $ python cchain.py -i 1tf6.pdb -c A B -l A
    
    args = read_options()
    c = CCHAIN(args.f_input, args.chains, args.letter)
    c.create_pdb_file()
    
    if (c.concatenated):
        c.create_json_file()
        c.create_csv_file()
    
    #print (c.get_full_output_filename())
    #print (c.get_output_filename())
