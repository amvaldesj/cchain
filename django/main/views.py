from django.shortcuts import render
from django.conf import settings

# Create your views here.
def home(request):
    return render(request, "home.html", {'settings': settings})

def upload(request):
    return render(request, "upload.html", {'settings': settings})

def chains(request, *args, **kwargs):
    action = int(kwargs['action'])
    
    # upload PDB file.
    if action == 1:
        if request.FILES:
            from django.core.files.storage import FileSystemStorage
            
            id_folder, tmp_folder = create_folder()
            data = {}
            data["tmp_folder"] = tmp_folder
            data["id_folder"] = id_folder
                
            myfile = request.FILES["pdbfile"]
            fs = FileSystemStorage(location=data["tmp_folder"])
            filename = fs.save(myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            data["pdbfile"] = filename
            
            get_chains(data)
            
            return render(request, "chains.html", {'settings': settings, 'data': data})
    
    # concatenate selected chains.
    elif action == 2:
        from django.http import HttpResponse
        import json
        from modules.cchain import CCHAIN
        
        pdbfile = request.POST.get("pdbfile")
        checkchain = request.POST.getlist("checkchain")
        tmp_folder = request.POST.get("tmp_folder")
        id_folder = request.POST.get("id_folder")

        if (len(checkchain) > 0):
            c1 = CCHAIN(tmp_folder + "/" + pdbfile, checkchain, 'A')
            c1.create_pdb_file()
            c1.create_json_file()
            c1.create_csv_file()
            new_pdb = c1.get_output_filename()
            
            host = request.get_host()
                
            if (request.is_secure()):
                host = "https://" + host 
            else:
                host = "http://" + host
                    
            results = {'new_pdb': new_pdb, 'original_pdb': pdbfile, 'results_url': host + settings.RESULTS_URL + id_folder + "/", 'concatenated': c1.concatenated, 'uniq_chain': c1.uniq_chain}
            
            return HttpResponse(json.dumps(results), content_type='application/json')

        else:
            return render(request, "chains.html", {'settings': settings})
    else:
        return render(request, "upload.html", {'settings': settings})
    
def create_folder():
    import time
    import os
    
    # creates temporal folder.
    current_milli_time = lambda: int(round(time.time() * 1000))
    id_folder = str(current_milli_time())
    tmp_folder = settings.RESULTS_ROOT + "/" + id_folder
    os.mkdir(tmp_folder)    
    
    return id_folder, tmp_folder

def get_chains(data):
    from Bio.PDB.PDBParser import PDBParser
    
    pdb_file = data["tmp_folder"] + "/" + data["pdbfile"]
    
    # reads PDB's structure.
    parser = PDBParser()
    structure = parser.get_structure(pdb_file, pdb_file)
    
    # ALWAYS THE FIRST MODEL.
    model = structure[0]

    all_chain = []
    for chain in structure.get_chains():
        if chain.get_parent().get_id() == 0:
            if chain.get_id() == ' ':
                all_chain.append('empty')
            else:
                all_chain.append(chain.get_id())
                
    all_chain.sort()
    data["chains"] = all_chain
