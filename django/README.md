
* install packages:

libapache2-mod-wsgi-py3

-----------------------------------------------------
# https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/modwsgi/#daemon-mode

<VirtualHost *:80>
    ServerName localhost

    WSGIDaemonProcess cchain threads=5 python-path=/home/amvaldesj/public_html/cchain:/home/amvaldesj/public_html/venv-python/lib/python3.7/site-packages
    WSGIScriptAlias /cchain /home/amvaldesj/public_html/cchain/cchain/wsgi.py  process-group=cchain
    <Directory /home/amvaldesj/public_html/cchain/cchain/>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>
    WSGIProcessGroup cchain
    Alias /cchain/static/ "/home/amvaldesj/public_html/cchain/static/cchain/"
    <Directory /home/amvaldesj/public_html/cchain/static/cchain/>
        Require all granted
    </Directory>
    Alias /cchain/results/ "/home/amvaldesj/public_html/cchain/results/"
    <Directory /home/amvaldesj/public_html/cchain/results/>
        Require all granted
    </Directory>

</VirtualHost>

-----
