$(document).ready(function () {
    $('#li-upload').toggleClass('active');
    $("#spinner-line").hide();
    
    let viewer1 = init_viewer('#container-01');
    let viewer2 = init_viewer('#container-02');
    
    $("#3dview").hide();
    $("#map").hide();
    
    let frm = $('#formParams');
    frm.submit(function () {
        let checked = 0;
        $('input[name="checkchain"]:checked').each(function() {
            checked++;
        });
        
        if (checked < 1) {
            swal('Atention', "Select at least one chains", 'warning');
            return false;
        }
        
        $("#spinner-line").show();
        
        $.ajax({
            async: true,
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: "json",
            success: function (data) {

                let original_pdb = data.results_url + data.original_pdb;
                let new_pdb      = data.results_url + data.new_pdb + ".pdb";
                let new_map_json = data.results_url + data.new_pdb + ".json";
                let new_map_csv  = data.results_url + data.new_pdb + ".csv";
                
                show_viewer(original_pdb, viewer1, 1, 'chain');
                show_viewer(new_pdb, viewer2, 2, 'red');
                
                $("#span-ori").html(data.original_pdb);
                $("#span-new").html(data.new_pdb + ".pdb");
                
                $("#aori").attr("href", original_pdb);
                $("#anew").attr("href", new_pdb);
                $("#amap").attr("href", new_map_csv);
                $("#jmap").attr("href", new_map_json);
                
                $("#tmap").empty();
                $.getJSON(new_map_json, function(data) {
                    let first = false;
                    
                    $('#tmap').append( '<tr>');
                    $.each( data, function(key, val) {
                        let tmp = "<td><table align='center'><tr><th colspan='3'>Chain: " + key + "</th></tr>";
                        
                        val.forEach(function(element) {
                            tmp += "<tr><td>" + element.origin + "</td><td> -> </td><td>" + element.new + "</td></tr>"
                        });
                        tmp += "</table></td>";
                        $('#tmap').append(tmp);
                    });
                    $('#tmap').append( '</tr>');
                });
                
                $("#3dview").show();
                
                if (data.concatenated == true)
                    $("#map").show();
                else
                    $("#map").hide();
            },
            error: function(data) {
                swal('Atention', "Occurred an error.", 'error');
            }
        });
        $("#spinner-line").hide();
        return false;
    });
});

function show_viewer(pdb, viewer, colorschemespec, type) {
    $.ajax(pdb, { 
        success: function(data) {
            viewer.clear();
            viewer.addModel(data, "pdb");
            //https://3dmol.csb.pitt.edu/doc/types.html#ColorschemeSpec
            if (colorschemespec==1) viewer.setStyle({}, {stick: {colorscheme: type}});
            if (colorschemespec==2) viewer.setStyle({}, {stick: {color: type}});
            viewer.render();
            viewer.zoomTo();
        },
        error: function(hdr, status, err) {
            console.error( "Failed to load PDB " + pdb + ": " + err );
        },
    });
}

function init_viewer(container_id) {
    let container = $(container_id);
    let config = { backgroundColor: 'black' };
    let viewer = $3Dmol.createViewer(container, config);
    viewer.render();
    return viewer;
}
